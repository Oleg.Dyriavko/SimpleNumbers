package com.company;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FindTest {

    @Test
    public void shouldReturnCollection() {
        Find find = new Find();
        Integer[] array = {2, 3, 5, 7, 11, 13};

        Assert.assertEquals("Should return range from 2 to 15",
                Arrays.asList(array), find.findAllPrimeNumbers("2", "15"));

    }@Test
    public void shouldReturnCollectionALessB() {
        Find find = new Find();
        Integer[] array = {2, 3, 5, 7, 11, 13};

        Assert.assertEquals("Should return range from 2 to 15",
                Arrays.asList(array), find.findAllPrimeNumbers("15", "2"));

    }
}
