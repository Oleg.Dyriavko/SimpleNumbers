package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class Find {


    public List<Integer> findAllPrimeNumbers(String one, String two) {
        Integer a = Integer.parseInt(one);
        Integer b = Integer.parseInt(two);

        boolean[] grid = new boolean[b];

        List<Integer> list = new ArrayList<>();

        for (int i = 0; i < b; i++) {
            grid[i] = true;
        }


        if (a > 1 && b > 1)

        {

            if (a < b) {
                for (int i = 2; i * i < b; i++) {
                    if (grid[i] == true) {
                        for (int j = i * i; j < b; j += i) {
                            grid[j] = false;
                        }
                    }
                }
                for (int i = a; i < b; i++) {
                    if (grid[i]) {
                        list.add(i);
                    }
                }
                return list;
            }
            if (a > b) {
                for (int i = 2; i * i < a; i++) {
                    if (grid[i] == true) {
                        for (int j = i * i; j < a; j += i) {
                            grid[j] = false;
                        }
                    }
                }
                for (int i = b; i < a; i++) {
                    if (grid[i]) {
                        list.add(i);
                    }
                }
                return list;
            }

        } else System.out.println("Error You entered a number less than zero ");
        return list;

    }

    public void print(List<Integer> list) {

        list.forEach(number -> System.out.println(number));
    }

}